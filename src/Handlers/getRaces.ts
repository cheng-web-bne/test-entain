import axios from 'axios';

const getRaces = () => 
  axios.get('https://api.neds.com.au/rest/v1/racing/?method=nextraces&count=10');
  
export default getRaces;