import React, { useEffect, useState } from 'react';
import styles from './index.module.scss';
import { FireOutlined } from '@ant-design/icons';
import { IRaceBase } from '../../interfaces/race';
import { createUseStyles } from 'react-jss';
import bg from '../../images/bg-race.jpeg';
import _ from 'lodash';

interface RaceCardProps {
	race: IRaceBase | {};
	initialTime: number;
	index: number,
}

/**
 * @description A card shows the details of a race
 * @param {IRaceBase} race the race data to be displayed
 */
const RaceCard: React.FC<RaceCardProps> = ({ race, initialTime }) => {
	const useStyles = createUseStyles({
		card: {
			backgroundImage: `url(${bg})`, 
		}
	});
	const classes = useStyles();

	const [timer, setTimer] = useState<Array<string>>([]);
	const [timeValue, setTimeValue] = useState<number>(0);
	let countDownTimer: ReturnType<typeof setTimeout>;

	useEffect(() => {
		if ((race as IRaceBase)?.advertised_start?.seconds) {
			setTimeValue((race as IRaceBase)?.advertised_start?.seconds - initialTime);
		}
	}, [(race as IRaceBase)?.advertised_start?.seconds])

	useEffect(() => {
		const emptyArray = ['-', '-', '-', '-', '-']
		if (!isNaN(timeValue)) {
			const countDown = new Date(timeValue * 1000).toISOString().substr(14, 5).split('');
			setTimer([timeValue > 0 ? 'In' : ''].concat(timeValue > 0 ? countDown : emptyArray));
		}
	}, [timeValue])

	useEffect(() => {
		if (timeValue && !isNaN(timeValue)) {
			countDownTimer = setTimeout(
				() => setTimeValue(timeValue - 1), 1000
			);
		}
  });

	useEffect(() => {
		return () => clearTimeout(countDownTimer);
	}, []);

	const renderCardFooter = (link?: string): JSX.Element => {
		return (
			<div className={styles['card__footer-container']}>
				<FireOutlined />
				<span className={styles['card__footer-text']}>
					View race details
				</span>
				<a href={link ? link : '#'}>
					<span className={styles['card__footer-link']}>View</span>
				</a>
			</div>
		)
	};
	return (
		<div className={styles['card-wrapper']}>
			<div className={`${styles['card']} ${classes.card}`}>
				<h1 className={styles['card__title']}>
					<span className={styles['card__title-text']}>
						{!_.isEmpty(race) && (race as IRaceBase).meeting_name}
					</span>
				</h1>
				<div className={styles['card__timer']}>
					{timer.map((item, i) => (
						<span key={i} className={styles['card__timer-number']}>
							<div className={styles['card__timer-container']}>{item}</div>
							<div className={styles['card__timer-decoration']}>
								<p>I</p>
								<p>I</p>
							</div>
						</span>
					))}
				</div>
				<div className={styles['card__details']}>
					{!_.isEmpty(race) && [(race as IRaceBase).race_name.substring(0, 38)].map((item, i) => (
						<h3 key={i} className={styles['card__details-text']}>{item}</h3>
					))}
					<div className={styles['card__info']}>
						Race number: {!_.isEmpty(race) && (race as IRaceBase)?.race_number}
					</div>
					<section className={styles['card__info-text']}>
						Track condition: {!_.isEmpty(race) && (race as IRaceBase)?.race_form?.track_condition?.name}
					</section>
					<section className={styles['card__info-text']}>
						Weather condition: {!_.isEmpty(race) && (race as IRaceBase)?.race_form?.weather?.name}
					</section>
					
				</div>
			</div>
			<div className={styles['card__footer']}>
				{renderCardFooter()}
			</div>
		</div>
	)
}

export default RaceCard;
