import React, { useEffect, useState } from 'react';
import {
	IRaces,
	IRaceBase,
} from '../../interfaces/race';
import RaceCard from './RaceCard';
import styles from './index.module.scss';
import getRaces from '../../Handlers/getRaces';
import _ from 'lodash';
import { Checkbox } from 'antd';

interface RaceCardListProps {
	races?: Array<IRaceBase>; //should not be optional
}

/**
 * @description Smart component fetching all race data and send to individual race card component
 * @param {Array<IRaceBase>} races the race data to be sent to RaceCard
 */

const RaceCardList: React.FC<RaceCardListProps> = ({}): JSX.Element => {
	const [res, setRes] = useState<IRaces>();
	const [sortedRaces, setSortedRaces] = useState<Array<IRaceBase>>([]);
	const [timeValue, setTimeValue] = useState<number>(0);
	const [selectedCategories, setSelectedCategories] = useState<Array<string>>([]);

	useEffect(() => {
		if(_.isEmpty(selectedCategories)) {
			setSelectedCategories([
				'9daef0d7-bf3c-4f50-921d-8e818c60fe61', 
				'161d9be2-e909-4326-8c2c-35ed71fb460b', 
				'4a2788f8-e825-4d36-9894-efd4baf1cfae',
			])
		}
	}, [selectedCategories]);
	
	useEffect(() => {
		try {
			getRaces().then(
				response => {
					setRes(response.data);
				}
			);
		} catch(e) {
			console.error(e);
		} 
	}, []);

	const races: Array<IRaceBase | {}> = Object.values(res?.data?.race_summaries ?? {})
		.sort((a, b) =>(a as IRaceBase)?.advertised_start?.seconds - (b as IRaceBase)?.advertised_start?.seconds);
	const initialTime = (races[0] as IRaceBase)?.advertised_start?.seconds - 60;
	
	useEffect(() => {
		setTimeValue(initialTime);
	}, [initialTime]);

	useEffect(() => {
		if (!_.isEmpty(races) && timeValue) {
			const filteredRaces = races.filter(race => {
				return (race as IRaceBase)?.advertised_start?.seconds - timeValue >= -60
					&& selectedCategories.includes((race as IRaceBase)?.category_id);
			});
			setSortedRaces((filteredRaces as Array<IRaceBase>).slice(0,5));
		}
	}, [timeValue]);

	useEffect(() => {
		if (timeValue && !isNaN(timeValue)) {
			setTimeout(
				() => setTimeValue(timeValue + 1), 1000
			);
		}
  });

	const options = [
		{ label: '● Greyhound racing',value: '9daef0d7-bf3c-4f50-921d-8e818c60fe61' },
		{ label: '● Harness racing', value: '161d9be2-e909-4326-8c2c-35ed71fb460b' },
		{ label: '● Horse racing', value: '4a2788f8-e825-4d36-9894-efd4baf1cfae' },
	];

	const onChange = (event: any) => {
		setSelectedCategories(event);
	};

	return (
		<>
			<Checkbox.Group options={options} onChange={onChange} />
			<div className={styles['race-list']}>
				{sortedRaces.map((item, i)=> (
					<RaceCard key={i} index={i} race={item} initialTime={initialTime} />
				))}
			</div>
		</>
	)
}

export default RaceCardList;
